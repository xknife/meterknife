﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKnife.Circe.Base
{
    public enum LogTargetEnum
    {
        All,
        Runtime,
        ExpCsvFile
    }
}
