﻿namespace NKnife.Circe.Base
{
    /// <summary>
    /// 接驳器。描述一个主机与外部设备之间的连接器。可以是一个物理连接器，也可以是一个虚拟连接器。例如：Agilent82357A，或者GPIB、USB、RS232、TCP/IP等。
    /// </summary>
    public interface IConnector { }
}