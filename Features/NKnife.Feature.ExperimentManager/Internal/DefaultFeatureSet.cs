﻿using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using NKnife.Feature.ExperimentManager.Internal.View;
using NKnife.Feature.ExperimentManager.Internal.ViewModel;
using NLog;
using RAY.Common;
using RAY.Common.Authentication;
using RAY.Common.Enums;
using RAY.Common.UI;
using RAY.Library;
using RAY.Plugins.WPF.Common;
using RAY.Plugins.WPF.Ribbons;

namespace NKnife.Feature.ExperimentManager.Internal
{
    class DefaultFeatureSet() : BaseFeatureSet
    {
        private static readonly ILogger s_logger = LogManager.GetCurrentClassLogger();

        private IUIManager? _uiManager;

        private ICommand ShowExperimentManagerWindowVm => new RelayCommand(() =>
        {
            _uiManager!.ShowDialog(new ExperimentManagerWindowVm()
            {
                Title = "实验管理",ContentId = Guid.NewGuid().ToString()
            });
        });

        /// <inheritdoc />
        protected override Dictionary<string, ICommand> RegisterCommandDictionary()
        {
            var commands = new Dictionary<string, ICommand> { { nameof(ShowExperimentManagerWindowVm), ShowExperimentManagerWindowVm } };
            return commands;
        }

        /// <inheritdoc />
        public override void Inject(IUIManager uiManager)
        {
            _uiManager = uiManager;
            uiManager.WorkbenchInitialized += UiManagerWorkbenchInitialized;
        }

        private void UiManagerWorkbenchInitialized(object? sender, EventArgs e)
        {
            if (FocalPoints == null) return;
            foreach (var focalPointPair in FocalPoints)
            {
                var focalPoint = focalPointPair.Value;

                var hasPermission = PermissionManager.HasPermission([
                    RoleType.SuperAdmin,
                    RoleType.Admin
                ]);

                focalPoint.SetEnable(hasPermission);
            }
        }

        /// <inheritdoc />
        public override IEnumerable<VmPair> BuildPaneModel()
        {
            return [];
        }

        /// <inheritdoc />
        public override IEnumerable<VmPair> BuildDialogModel()
        {
            return [new VmPair(View: typeof(ExperimentManagerWindow), ViewModel: typeof(ExperimentManagerWindowVm))];
        }
    }
}