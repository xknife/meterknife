﻿using RAY.Common.UI;

namespace NKnife.Module.Workbench.Internal.View
{
    /// <summary>
    /// Interaction logic for Workbench.xaml
    /// </summary>
    public partial class Workbench : IWorkbench
    {
        public Workbench()
        {
            InitializeComponent();
        }

        public void Dispose()
        {
        }
    }
}