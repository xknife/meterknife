﻿using RAY.Common.Plugin.Modules;

namespace NKnife.Module.UutManager.Internal
{
    internal class DefaultModuleContext : BaseModuleContext
    {
        #region Overrides of BaseModuleContext
        /// <inheritdoc />
        public override void Initialize() { }
        #endregion
    }
}